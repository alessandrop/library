module.exports = app => {
    const items = require("../controllers/item.controller.js");
  
    // Create a new Item
    app.post("/items", items.create);
  
    // Retrieve all Item
    app.get("/items", items.findAll);
  
    // Retrieve a single Item with itemId
    app.get("/items/:itemId", items.findOne);
  
    // Update a Item with itemId
    app.put("/items/:itemId", items.update);
  
    // Delete a Item with itemId
    app.delete("/items/:itemId", items.delete);
  
    // Create a new Item
    app.delete("/items", items.deleteAll);

    app.patch("/items/:itemId/lent-to", items.changeLento)

    app.put("/items/:itemId/give-back", items.giveBack)
  };