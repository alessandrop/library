const express = require('express')

const app = express()
var port = 3001

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

require("./routes/items.routes.js")(app);

app.use('/', express.static('frontend/build'));
app.use('/library', express.static('frontend/build'));

const args = process.argv.slice(2)

if (args.includes("-port")) {
  const index = args.indexOf("-port")
  if (args[index+1] != null) {
    port = args[index+1]
    console.log("Using port " + port)
  }
} else {
  console.log("No port specified with the -port argument - using the default " + port)
}

app.listen(port, () => {
    console.log("Server is running on port " + port)
})