const sql = require("../db.js");

// constructor
const Item = function (item) {
  this.name = item.name
  this.author = item.author
  this.keywords = item.keywords
  this.description = item.description
  this.lentTo = item.lentTo
  this.imageUrl = item.imageUrl
  this.link = item.link
  this.isbn = item.isbn
};

Item.create = (newItem, result) => {
  sql.query("INSERT INTO items SET ?", newItem, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created item: ", { id: res.insertId, ...newItem });
    result(null, { id: res.insertId, ...newItem });
  });
};

Item.findById = (itemId, result) => {
  sql.query(`SELECT * FROM items WHERE id = ${itemId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found item: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Item with the id
    result({ kind: "not_found" }, null);
  });
};

Item.getAll = result => {
  sql.query("SELECT * FROM items", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("items: ", res);
    result(null, res);
  });
};

Item.updateById = (id, item, result) => {
  sql.query(
    "UPDATE items SET name = ?, imageUrl = ?, author = ?, description = ?, keywords = ?, link = ?, lentTo = ?, isbn = ? WHERE id = ?",
    [item.name, item.imageUrl, item.author, item.description, item.keywords, item.link, item.lentTo, item.isbn, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found item with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated item: ", { id: id, ...item });
      result(null, { id: id, ...item });
    }
  );
};

Item.changeLento = (id, lenterName, result) => {
  sql.query(
    "UPDATE items SET lentTo = ? WHERE id = ?", [lenterName, id], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found item with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated lenter name: ", { id: id, lenterName });
      result(null, { id: id, lenterName });
    }
  )
}

Item.giveBack = (id, result) => {
  sql.query(
    "UPDATE items SET lentTo = NULL WHERE id = ?", [id], (err, res) => {
      if (err) {
        console.log("error: ", err)
        result(null, err)
        return
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("Gave book back with id: ", { id: id});
      result(null, { id: id});
    }
  )
}

Item.remove = (id, result) => {
  sql.query("DELETE FROM items WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Item with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted Item with id: ", id);
    result(null, res);
  });
};

Item.removeAll = result => {
  sql.query("DELETE FROM items", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} items`);
    result(null, res);
  });
};

module.exports = Item;