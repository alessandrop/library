import React, { Component } from 'react'
import Items from './components/items'
import axios from 'axios'

class App extends Component {

  state = {
    items: []
  }

  componentDidMount() {
    this.loadItems()
  }
  
  loadItems = () => {
    fetch("/items")
    .then((res) => res.json())
    .then((data) => {
      this.setState({ items: data})
    })
    .catch(console.log)
  }

  handleClick = (id) => {
    const name = prompt("Your name?")
    if (name === null) {
      return
    }
    this.submitLenter(id, name)
  };

  submitLenter = (id, name) => {
    console.log("submitLenter() called with params: ", id, name)
    const param = { "lentTo": name }
    axios.patch("/items/" + id + "/lent-to", param)
      .then(response => {
        this.loadItems()
      })
  }

  giveBack = (id) => {
    console.log("giving back book with id: ", id)
    axios.put("/items/" + id + "/give-back")
      .then(response => {
        this.loadItems()
    })
  }
  
  render() {
    return (
      <div>
      <p><Items items={this.state.items} onBorrow={this.handleClick} onGiveBack={this.giveBack}/></p>
      </div>
    )
  }
}

export default App;
