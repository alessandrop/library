import React, { Component } from 'react'
import '../Item.css';

class Items extends Component {

  createButton = (item) => {
    if (item.lentTo == null) {
      return <button class="card-btn" onClick={() => this.props.onBorrow(item.id)}>Borrow</button>
    } else {
      return <button class="card-btn-lent" onClick={() => this.props.onGiveBack(item.id)}>Borrowed to {item.lentTo} (Give back)</button>
    }
  }

  render() {
    return (
      <div>
        <div class="title">Welcome to the Kontomanager library</div>
        {this.props.items.map((item, index) => (
  
          <div class="card-container">
            <div class="card u-clearfix">
              <div class="card-body">
                <div class="card-title">{item.name}</div>
                <span class="card-author subtle">{item.author}</span>
                <span class="card-description subtle">{item.description.substring(0, 800)}</span>
                {this.createButton(item)}
              </div>
              <a href={item.link} target="_blank" rel="noreferrer"><img src={item.imageUrl} alt={item.imageUrl} class="card-media" /></a>
            </div>
            <div class="card-shadow"></div>
          </div>
  
        ))}
      </div>
    )
  }
}

export default Items