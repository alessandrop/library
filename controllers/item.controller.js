const Item = require("../models/item.model.js");

// Create and Save a new Item
exports.create = (req, res) => {

    if (!req.body) {
        res.status(400).send ({
            message: "Body cannot be empty"
        })
    }

    const item = new Item({
        name: req.body.name,
        author: req.body.author,
        keywords: req.body.keywords,
        description: req.body.description, 
        imageUrl: req.body.imageUrl
    })

    Item.create(item, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "item could not be saved"
            })
        } else {
            res.send(data)
        }
    })  
};

// Retrieve all Items from the database.
exports.findAll = (req, res) => {

    Item.getAll((err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Could not retrieve items from database"
            })
        } else {
            res.send(data)
        }
    })
};

// Find a single Item with a itemId
exports.findOne = (req, res) => {
    Item.findById(req.params.itemId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Couldn't find Item with id ${req.params.itemId}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Item with id " + req.params.itemId
            });
          }
        } else res.send(data);
      });
};

// Update a Item identified by the itemId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
          message: "Body can not be empty!"
        });
      }
    
      Item.updateById(req.params.itemId, new Item(req.body), (err, data) => {
          if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Not found Item with id ${req.params.itemId}.`
              });
            } else {
              res.status(500).send({
                message: "Error updating Item with id " + req.params.itemId
              });
            }
          } else res.send(data);
        }
      );
};

// Delete a Item with the specified itemId in the request
exports.delete = (req, res) => {
    Item.remove(req.params.itemId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Item with id ${req.params.itemId}.`
            });
          } else {
            res.status(500).send({
              message: "Could not delete Item with id " + req.params.itemId
            });
          }
        } else res.send({ message: `Item was deleted successfully!` + data });
      });
};

// Delete all Item from the database.
exports.deleteAll = (req, res) => {
    Item.removeAll((err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Could not delete all items"
            })
        } else {
            res.end({ message: "All items deleted" + data})
        }
    })
};

exports.changeLento = (req, res) => {
    if (!req.body || req.body.lentTo == null) {
      res.status(400).send({
        message: "Body has to contain lenter name"
      });
    }

    Item.changeLento(req.params.itemId, req.body.lentTo, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Item with id ${req.params.itemId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating Item with id " + req.params.itemId
          });
        }
      } else res.send(data);
    })
}

exports.giveBack = (req, res) => {
  Item.giveBack(req.params.itemId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Item with id ${req.params.itemId}.`
        });
      } else {
        res.status(500).send({
          message: "Error updating Item with id " + req.params.itemId
        });
      }
    } else res.send(data);
  })
}